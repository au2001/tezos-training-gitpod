tezos-client remember contract ex1 KT1CENSNCADcnQUauEiTBNYcSabxcYBAKwsJ
tezos-client --wait none transfer 0 from aurelien to ex1 --arg '1'

# https://better-call.dev/ghostnet/KT1CENSNCADcnQUauEiTBNYcSabxcYBAKwsJ/operations

tezos-client remember contract ex2 KT1L8cMTbxGtQnqYQ2sA555cBLLuVfFxoRrM
tezos-client --wait none transfer 0 from aurelien to ex2 --arg '"Test"' --burn-cap 0.00125

# https://better-call.dev/ghostnet/KT1L8cMTbxGtQnqYQ2sA555cBLLuVfFxoRrM/operations
