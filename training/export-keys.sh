for user in "$@" 
do
    tezos-client show address $user -S | grep -oh "unencrypted:.*" >> /workspace/training-gitpod/secret_keys;
done
