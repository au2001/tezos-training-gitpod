tezos-client gen keys alice
tezos-client show address alice -S

# Secret Key: unencrypted:edsk3LjGuNN9NWnwDqk8xLkCBZXEoG1gvTFsRmHjxDJfDTTE9UHkFj

completium-cli import privatekey 'edsk3LjGuNN9NWnwDqk8xLkCBZXEoG1gvTFsRmHjxDJfDTTE9UHkFj' as alice
