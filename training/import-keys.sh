#!/bin/bash
input="/workspace/training-gitpod/secret_keys"
while read -r line
do
  tezos-client import secret key $1 $line;
  shift 1;
done < "$input"
