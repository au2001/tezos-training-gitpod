source config.sh

tezos-client gen keys aurelien
tezos-client show address aurelien

# tz1enmvx5FgZSoyGxWSG6ai9vprHjS6TZVqZ

# https://ghostnet.tzkt.io/tz1enmvx5FgZSoyGxWSG6ai9vprHjS6TZVqZ/operations/

# https://faucet.ghostnet.teztnets.xyz

tezos-client get balance for aurelien
