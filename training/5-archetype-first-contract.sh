completium-cli create project tezos-contracts

# ... Write contracts ...

completium-cli run binder-ts

# ... Write tests ...

npm run test
