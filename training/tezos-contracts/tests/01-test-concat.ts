import assert from 'assert'
import { concat } from './binding/concat'

describe('[CONCAT] Contract deployment', async () => {
  it('Deploy concat', async () => {
    await concat.deploy({})
  })
})

describe('[CONCAT] Call entry', async () => {
  it("Call 'add'", async () => {
    const s_before = await concat.get_text()
    assert(s_before === "")

    await concat.add("hello", {})

    const s_between = await concat.get_text()
    assert(s_between == ",hello")

    await concat.add("world", {})

    const s_after = await concat.get_text()
    assert(s_after == ",hello,world")
  })
})
