import { expect_to_fail, get_account, Nat, set_mockup, set_mockup_now, string_to_mich, Tez } from '@completium/experiment-ts'
import assert from 'assert'
import { visitors } from './binding/visitors'

const bootstrap1 = get_account('bootstrap1')

set_mockup()

const error_first_visit = string_to_mich("you must pay 5tz on first visit")
const error_subsequent_visit = string_to_mich("you must pay 3tz per subsequent visit")

describe('[COMMUNITY_COUNT] Contract deployment', async () => {
  it('Deploy visitors', async () => {
    await visitors.deploy({})
  })
})

describe('[COMMUNITY_COUNT] Call entry', async () => {
  it("Call 'visit' before registering", async () => {
    await expect_to_fail(async () => {
      await visitors.visit({ as: bootstrap1, amount: new Tez(5) })
    }, visitors.errors.YOU_MUST_REGISTER_FIRST)
  })

  it("Call 'register'", async () => {
    const s_before = await visitors.get_visitor()
    assert(s_before.length === 0)

    await visitors.register("Test", { as: bootstrap1 })

    const s_after = await visitors.get_visitor()
    assert(s_after.length === 1)
    const [[l_after, v_after]] = s_after
    assert(l_after.equals(bootstrap1.get_address()))
    assert(v_after.name === "Test")
    assert(v_after.nbvisits.equals(new Nat(0)))
  })

  it("Call 'register' again", async () => {
    await visitors.register("Alice", { as: bootstrap1 })

    const s_after = await visitors.get_visitor()
    assert(s_after.length === 1)
    const [[l_after, v_after]] = s_after
    assert(l_after.equals(bootstrap1.get_address()))
    assert(v_after.name === "Alice")
    assert(v_after.nbvisits.equals(new Nat(0)))
  })

  it("Call 'visit' with an incorrect amount", async () => {
    await expect_to_fail(async () => {
      await visitors.visit({ as: bootstrap1, amount: new Tez(3) })
    }, error_first_visit)
  })

  it("Call 'visit'", async () => {
    await visitors.visit({ as: bootstrap1, amount: new Tez(5) })

    const s_after = await visitors.get_visitor()
    assert(s_after.length === 1)
    const [[l_after, v_after]] = s_after
    assert(l_after.equals(bootstrap1.get_address()))
    assert(v_after.nbvisits.equals(new Nat(1)))
  })

  it("Call 'visit' again too soon", async () => {
    await expect_to_fail(async () => {
      await visitors.visit({ as: bootstrap1, amount: new Tez(3) })
    }, visitors.errors.r2)
  })

  it("Call 'visit' again with an incorrect amount", async () => {
    const s_before = await visitors.get_visitor()
    const a_before = s_before.find(([user]) => user.equals(bootstrap1.get_address()))
    assert(a_before !== undefined)
    const [_, v_before] = a_before

    const future = new Date(v_before.last_visit.getTime())
    future.setDate(future.getDate() + 12)
    set_mockup_now(future)

    await expect_to_fail(async () => {
      await visitors.visit({ as: bootstrap1, amount: new Tez(2) })
    }, error_subsequent_visit)
  })

  it("Call 'visit' again", async () => {
    await visitors.visit({ as: bootstrap1, amount: new Tez(3) })

    const s_after = await visitors.get_visitor()
    assert(s_after.length === 1)
    const [[l_after, v_after]] = s_after
    assert(l_after.equals(bootstrap1.get_address()))
    assert(v_after.nbvisits.equals(new Nat(2)))
  })
})
