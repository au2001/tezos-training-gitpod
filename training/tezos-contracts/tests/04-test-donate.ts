import { expect_to_fail, get_account, Nat, set_mockup, set_mockup_now, Tez } from '@completium/experiment-ts'
import assert from 'assert'
import { donate } from './binding/donate'

const bootstrap2 = get_account('bootstrap2')

set_mockup()

describe('[COMMUNITY_COUNT] Contract deployment', async () => {
  it('Deploy donate', async () => {
    await donate.deploy(bootstrap2.get_address(), new Nat(50), {})
  })
})

describe('[COMMUNITY_COUNT] Call entry', async () => {
  it("Call 'deposit'", async () => {
    const s_before = await donate.get_balance()
    assert(s_before.equals(new Tez(0)))

    await donate.deposit(new Nat(50), { amount: new Tez(120) })

    const s_between = await donate.get_balance()
    assert(s_between.equals(new Tez(120)))

    await donate.deposit(new Nat(50), { amount: new Tez(140) })

    const s_after = await donate.get_balance()
    assert(s_after.equals(new Tez(260)))
  })

  it("Call 'collect' with an amount too high", async () => {
    const max_collect = await donate.get_max_collect()
    assert(max_collect.equals(new Nat(50)))

    await expect_to_fail(async () => {
      await donate.collect(new Tez(200), { as: bootstrap2 })
    }, donate.errors.r1)
  })

  it("Call 'collect' from an unauthorized address", async () => {
    await expect_to_fail(async () => {
      await donate.collect(new Tez(1), {})
    }, donate.errors.INVALID_CALLER)
  })

  it("Call 'collect'", async () => {
    await donate.collect(new Tez(130), { as: bootstrap2 })

    const s_after = await donate.get_balance()
    assert(s_after.equals(new Tez(130)))
  })

  it("Call 'collect' too soon", async () => {
    await expect_to_fail(async () => {
      await donate.collect(new Tez(1), { as: bootstrap2 })
    }, donate.errors.r2)
  })

  it("Call 'collect' after a while", async () => {
    const future = await donate.get_last_collect()
    future.setMinutes(future.getMinutes() + 3)
    set_mockup_now(future)

    await donate.collect(new Tez(40), { as: bootstrap2 })

    const s_after = await donate.get_balance()
    assert(s_after.equals(new Tez(90)))
  })

  it("Call 'collect' with an amount above the contract balance", async () => {
    const future = await donate.get_last_collect()
    future.setMinutes(future.getMinutes() + 3)
    set_mockup_now(future)

    await expect_to_fail(async () => {
      await donate.collect(new Tez(100), { as: bootstrap2 })
    }, donate.errors.r1)
  })

  it("Call 'deposit' with an amount too low", async () => {
    await expect_to_fail(async () => {
      await donate.deposit(new Nat(50), { amount: new Tez(90) })
    }, donate.errors.r3)
  })

  it("Call 'deposit' to change max_collect", async () => {
    await donate.deposit(new Nat(50), { amount: new Tez(100) })
  })

  it("Call 'deposit' with too low max_collect", async () => {
    await expect_to_fail(async () => {
      await donate.deposit(new Nat(0), { amount: new Tez(100) })
    }, donate.errors.r4)
  })

  it("Call 'deposit' with too high max_collect", async () => {
    await expect_to_fail(async () => {
      await donate.deposit(new Nat(110), { amount: new Tez(100) })
    }, donate.errors.r5)
  })
})
