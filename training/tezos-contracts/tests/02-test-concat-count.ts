import { Nat } from '@completium/experiment-ts'
import assert from 'assert'
import { concat_count } from './binding/concat_count'

describe('[CONCAT_COUNT] Contract deployment', async () => {
  it('Deploy concat_count', async () => {
    await concat_count.deploy({})
  })
})

describe('[CONCAT_COUNT] Call entry', async () => {
  it("Call 'add'", async () => {
    const s1_before = await concat_count.get_text()
    assert(s1_before === "")
    const s2_before = await concat_count.get_count()
    assert(s2_before.equals(new Nat(0)))

    await concat_count.add("hello", {})

    const s1_between = await concat_count.get_text()
    assert(s1_between == ",hello")
    const s2_between = await concat_count.get_count()
    assert(s2_between.equals(new Nat(1)))

    await concat_count.add("world", {})

    const s1_after = await concat_count.get_text()
    assert(s1_after == ",hello,world")
    const s2_after = await concat_count.get_count()
    assert(s2_after.equals(new Nat(2)))
  })
})
