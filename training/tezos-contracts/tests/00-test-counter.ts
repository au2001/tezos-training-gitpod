import assert from 'assert'
import { Nat } from '@completium/experiment-ts'
import { counter } from './binding/counter'

describe('[COUNTER] Contract deployment', async () => {
  it('Deploy counter', async () => {
    await counter.deploy({})
  })
})

describe('[COUNTER] Call entry', async () => {
  it("Call 'increment'", async () => {
    const s_before = await counter.get_count()
    assert(s_before.equals(new Nat(2)))

    await counter.increment({})

    const s_after = await counter.get_count()
    assert(s_after.equals(new Nat(3)))
  })
})
