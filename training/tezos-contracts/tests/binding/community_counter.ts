import * as ex from "@completium/experiment-ts";
const increment_arg_to_mich = (amount: ex.Nat): ex.Micheline => {
    return amount.to_mich();
}
const decrement_arg_to_mich = (amount: ex.Nat): ex.Micheline => {
    return amount.to_mich();
}
const reset_arg_to_mich = (): ex.Micheline => {
    return ex.unit_mich;
}
export class Community_counter {
    address: string | undefined;
    get_address(): ex.Address {
        if (undefined != this.address) {
            return new ex.Address(this.address);
        }
        throw new Error("Contract not initialised");
    }
    async get_balance(): Promise<ex.Tez> {
        if (null != this.address) {
            return await ex.get_balance(new ex.Address(this.address));
        }
        throw new Error("Contract not initialised");
    }
    async deploy(owner: ex.Address, params: Partial<ex.Parameters>) {
        const address = await ex.deploy("./contracts/community_counter.arl", {
            owner: owner.toString()
        }, params);
        this.address = address;
    }
    async increment(amount: ex.Nat, params: Partial<ex.Parameters>): Promise<any> {
        if (this.address != undefined) {
            return await ex.call(this.address, "increment", increment_arg_to_mich(amount), params);
        }
        throw new Error("Contract not initialised");
    }
    async decrement(amount: ex.Nat, params: Partial<ex.Parameters>): Promise<any> {
        if (this.address != undefined) {
            return await ex.call(this.address, "decrement", decrement_arg_to_mich(amount), params);
        }
        throw new Error("Contract not initialised");
    }
    async reset(params: Partial<ex.Parameters>): Promise<any> {
        if (this.address != undefined) {
            return await ex.call(this.address, "reset", reset_arg_to_mich(), params);
        }
        throw new Error("Contract not initialised");
    }
    async get_owner(): Promise<ex.Address> {
        if (this.address != undefined) {
            const storage = await ex.get_storage(this.address);
            return new ex.Address(storage.owner);
        }
        throw new Error("Contract not initialised");
    }
    async get_last_user(): Promise<ex.Address> {
        if (this.address != undefined) {
            const storage = await ex.get_storage(this.address);
            return new ex.Address(storage.last_user);
        }
        throw new Error("Contract not initialised");
    }
    async get_count(): Promise<ex.Nat> {
        if (this.address != undefined) {
            const storage = await ex.get_storage(this.address);
            return new ex.Nat(storage.count);
        }
        throw new Error("Contract not initialised");
    }
    errors = {
        INVALID_CALLER: ex.string_to_mich("\"INVALID_CALLER\""),
        r3: ex.string_to_mich("\"amount cannot be higher than the current count\""),
        r2: ex.string_to_mich("\"you cannot increment twice in a row\""),
        r1: ex.string_to_mich("\"amount must be at most 10\"")
    };
}
export const community_counter = new Community_counter();
