import * as ex from "@completium/experiment-ts";
const add_arg_to_mich = (part: string): ex.Micheline => {
    return ex.string_to_mich(part);
}
export class Concat {
    address: string | undefined;
    get_address(): ex.Address {
        if (undefined != this.address) {
            return new ex.Address(this.address);
        }
        throw new Error("Contract not initialised");
    }
    async get_balance(): Promise<ex.Tez> {
        if (null != this.address) {
            return await ex.get_balance(new ex.Address(this.address));
        }
        throw new Error("Contract not initialised");
    }
    async deploy(params: Partial<ex.Parameters>) {
        const address = await ex.deploy("./contracts/concat.arl", {}, params);
        this.address = address;
    }
    async add(part: string, params: Partial<ex.Parameters>): Promise<any> {
        if (this.address != undefined) {
            return await ex.call(this.address, "add", add_arg_to_mich(part), params);
        }
        throw new Error("Contract not initialised");
    }
    async get_text(): Promise<string> {
        if (this.address != undefined) {
            const storage = await ex.get_storage(this.address);
            return storage;
        }
        throw new Error("Contract not initialised");
    }
    errors = {};
}
export const concat = new Concat();
