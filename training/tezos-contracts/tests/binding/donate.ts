import * as ex from "@completium/experiment-ts";
const collect_arg_to_mich = (amount: ex.Tez): ex.Micheline => {
    return amount.to_mich();
}
const deposit_arg_to_mich = (new_max_collect: ex.Nat): ex.Micheline => {
    return new_max_collect.to_mich();
}
export class Donate {
    address: string | undefined;
    get_address(): ex.Address {
        if (undefined != this.address) {
            return new ex.Address(this.address);
        }
        throw new Error("Contract not initialised");
    }
    async get_balance(): Promise<ex.Tez> {
        if (null != this.address) {
            return await ex.get_balance(new ex.Address(this.address));
        }
        throw new Error("Contract not initialised");
    }
    async deploy(owner: ex.Address, max_collect: ex.Nat, params: Partial<ex.Parameters>) {
        const address = await ex.deploy("./contracts/donate.arl", {
            owner: owner.toString(),
            max_collect: max_collect.toString()
        }, params);
        this.address = address;
    }
    async collect(amount: ex.Tez, params: Partial<ex.Parameters>): Promise<any> {
        if (this.address != undefined) {
            return await ex.call(this.address, "collect", collect_arg_to_mich(amount), params);
        }
        throw new Error("Contract not initialised");
    }
    async deposit(new_max_collect: ex.Nat, params: Partial<ex.Parameters>): Promise<any> {
        if (this.address != undefined) {
            return await ex.call(this.address, "deposit", deposit_arg_to_mich(new_max_collect), params);
        }
        throw new Error("Contract not initialised");
    }
    async get_owner(): Promise<ex.Address> {
        if (this.address != undefined) {
            const storage = await ex.get_storage(this.address);
            return new ex.Address(storage.owner);
        }
        throw new Error("Contract not initialised");
    }
    async get_max_collect(): Promise<ex.Nat> {
        if (this.address != undefined) {
            const storage = await ex.get_storage(this.address);
            return new ex.Nat(storage.max_collect);
        }
        throw new Error("Contract not initialised");
    }
    async get_last_collect(): Promise<Date> {
        if (this.address != undefined) {
            const storage = await ex.get_storage(this.address);
            return new Date(storage.last_collect);
        }
        throw new Error("Contract not initialised");
    }
    errors = {
        r5: ex.string_to_mich("\"you cannot increase the threshold above 100%\""),
        r4: ex.string_to_mich("\"you cannot lower the threshold below 1%\""),
        r3: ex.string_to_mich("\"you can only deposit at least 100tz\""),
        r2: ex.string_to_mich("\"you must wait 2 minutes between collects\""),
        r1: ex.string_to_mich("\"amount is above the allowed threshold\""),
        INVALID_CALLER: ex.string_to_mich("\"INVALID_CALLER\"")
    };
}
export const donate = new Donate();
