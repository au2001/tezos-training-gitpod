import { expect_to_fail, get_account, Nat } from '@completium/experiment-ts'
import assert from 'assert'
import { community_counter } from './binding/community_counter'

const bootstrap2 = get_account('bootstrap2')
const alice = get_account('alice')
const bob = get_account('bob')
const carl = get_account('carl')

describe('[COMMUNITY_COUNT] Contract deployment', async () => {
  it('Deploy community_counter', async () => {
    await community_counter.deploy(bootstrap2.get_address(), {})
  })
})

describe('[COMMUNITY_COUNT] Call entry', async () => {
  it("Call 'increment'", async () => {
    const s_before = await community_counter.get_count()
    assert(s_before.equals(new Nat(0)))

    await community_counter.increment(new Nat(5), { as: alice })

    const s_between = await community_counter.get_count()
    assert(s_between.equals(new Nat(5)))

    await community_counter.increment(new Nat(9), { as: bob })

    const s_after = await community_counter.get_count()
    assert(s_after.equals(new Nat(14)))
  })

  it("Call 'increment' with an amount too high", async () => {
    await expect_to_fail(async () => {
      await community_counter.increment(new Nat(12), {})
    }, community_counter.errors.r1)
  })

  it("Call 'increment' twice in a row", async () => {
    await community_counter.increment(new Nat(3), { as: carl })

    await expect_to_fail(async () => {
      await community_counter.increment(new Nat(7), { as: carl })
    }, community_counter.errors.r2)
  })

  it("Call 'decrement' from an unauthorized address", async () => {
    await expect_to_fail(async () => {
      await community_counter.decrement(new Nat(1), {})
    }, community_counter.errors.INVALID_CALLER)
  })

  it("Call 'reset' from an unauthorized address", async () => {
    await expect_to_fail(async () => {
      await community_counter.reset({})
    }, community_counter.errors.INVALID_CALLER)
  })

  it("Call 'decrement' with an amount higher than count", async () => {
    await expect_to_fail(async () => {
      const count = await community_counter.get_count()
      await community_counter.decrement(count.plus(new Nat(1)), { as: bootstrap2 })
    }, community_counter.errors.r3)
  })

  it("Call 'reset'", async () => {
    await community_counter.reset({ as: bootstrap2 })

    const s_after = await community_counter.get_count()
    assert(s_after.equals(new Nat(0)))
  })

  it("Call 'decrement'", async () => {
    await community_counter.increment(new Nat(5), {})

    const s_before = await community_counter.get_count()
    assert(s_before.equals(new Nat(5)))

    await community_counter.decrement(new Nat(3), { as: bootstrap2 })

    const s_after = await community_counter.get_count()
    assert(s_after.equals(new Nat(2)))
  })
})
